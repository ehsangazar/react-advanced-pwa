import Head from 'next/head'
import styles from '../styles/Home.module.css'
import { useEffect, useState } from 'react'
import { toast } from "react-toastify";


export default function Home() {
  const [posts,setPosts] = useState(null)
  const [online,setOnline] = useState(true)

  useEffect(()=>{
    fetchAPI()
  },[])

  const fetchAPI = async () => {
    const response = await fetch('http://localhost:3000/api/posts')
    setPosts(await response.json())
  }

  useEffect(()=>{
    navigator.serviceWorker.register('/sw.js',{
      scope: '/'
    }).then(()=>{
      console.log('Registered')
    })
  },[])

  useEffect(()=>{
    window.addEventListener('online',handleOnline)
    window.addEventListener('offline',handleOnline)
  })

  const handleOnline = () => {
    setOnline(navigator.onLine)
  }

  useEffect(()=>{
    if (online) {
      notifyMe("You are online");
      toast('You are online')
    }else {
      notifyMe("You are offline");
      toast('You are offline')
    }
  },[online])

  const notifyMe = (text) => {
    if (!("Notification" in window)) {
      console.log("This browser does not support desktop notification");
    }
    else if (Notification.permission === "granted") {
      new Notification(text);
    }
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        if (permission === "granted") {
          new Notification('Welcome to My Website');
        }
      });
    }
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>PWA APP</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to <a href="/">PWA</a>
        </h1>

        <p className={styles.description}>
          Coded in React Advanced by Ehsan Gazar
        </p>

        <div className={styles.grid}>
          {
            posts && posts.map((post) => (
              <a key={`posts-${post.id}`} href="#" className={styles.card}>
                <img src={post.cover} />
                <h3>{post.title}</h3>
                <p>{post.content}</p>
              </a>
            ))
          }
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
