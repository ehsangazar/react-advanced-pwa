var CACHE_NAME = "react-advanced-cache-v2";
var urlsToCache = ["/", "/favicon.ico", "/api/posts"];

self.addEventListener("install", function (event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      console.log("Opened cache");
      return cache.addAll(urlsToCache);
    })
  );
});


self.addEventListener("fetch", function (event) {
  console.log('event.request',event.request)
  event.respondWith(
    caches.match(event.request).then(function (response) {
      // Cache hit - return response
      if (response) {
        return response;
      }
      return fetch(event.request);
    })
  );

  if (
    event.request.url.includes("_next") ||
    event.request.url.includes("img-content")
  ) {
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.add(event.request);
    });
  }
});

self.addEventListener("activate", function (event) {
  var cacheAllowlist = [CACHE_NAME];

  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.map(function (cacheName) {
          if (cacheAllowlist.indexOf(cacheName) === -1) {
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});